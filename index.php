<?php
try
{
    $db = new PDO('mysql:host=localhost;dbname=jeu;charset=utf8', 'root', '');
}
catch (Exception $e)
{
    exit('Erreur : ' . $e->getMessage());
}
?>
<!DOCTYPE html>
<html>
<head>
    <title></title>
    <meta charset="utf-8">
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

    <!-- Optional theme -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">
</head>
<body>
<div class="container">
    <div class="row">
        <div class="col-md-6 col-md-offset-3">
            <?php
            /**
             * @param $classe
             * Auto-load des classes
             */
            function chargerClasse($classe)
            {
                require 'class/' . $classe . '.php';
            }

            spl_autoload_register('chargerClasse');

            /*
             * Début script des personnages
             */

            $perso2 = new Personnage([
                'id' => 1,
                'forcePerso' => 1,
                'degats' => 1,
                'niveau' => 1,
                'experience' => 1
            ]);

            $manager = new PersonnagesManager($db);
            //$manager->add($perso);
            $manager->update($perso2);
            echo '<pre>';
            //var_dump($manager->getList());
            echo '</pre>';

            $_SESSION['id'] = 1;
            $id = $_SESSION['id'];
            ?>
            L'experience du personnage <?= $manager->get($id)->getId(); ?> et de : <?= $manager->get(1)->getExperience(); ?>

            <table class="table">
                <tbody>
                <tr>
                    <thead>
                <th>id</th>
                <th>Nom</th>
                <th>Force</th>
                <th>Degats</th>
                <th>Niveau</th>
                <th>Experience</th>
                </thead>
                    <?php foreach ($manager->getList() as $p) { ?>
                    <td><?= $p->getId(); ?></td>
                    <td><?= $p->getNom(); ?></td>
                    <td><?= $p->getForcePerso(); ?></td>
                    <td><?= $p->getDegats(); ?></td>
                    <td><?= $p->getNiveau(); ?></td>
                    <td><?= $p->getExperience(); ?></td>
                </tr>
                <?php }?>
                </tbody>
            </table>

            <?php

            echo '</tbody></table>';
            ?>
        </div>
    </div>
</div>
<!-- Latest compiled and minified JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
</body>
</html>


