<?php
class Personnage
{
    private $_id;
    private $_nom;
    private $_forcePerso;
    private $_degats;
    private $_niveau;
    private $_experience;

    /**
     * Personnage constructor.
     * @param array $donnees
     */
    public function __construct(array $donnees)
    {
        if(!empty($donnees))
            $this->hydrate($donnees);
    }

    /**
     * @param array $donnees
     */
    public function hydrate(array $donnees)
    {
        foreach ($donnees as $key => $value)
        {
            $method = 'set'.ucfirst($key);

            if (method_exists($this, $method))
            {
                $this->$method($value);
            }
        }
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->_id;
    }

    /**
     * @return mixed
     */
    public function getNom()
    {
        return $this->_nom;
    }

    /**
     * @return mixed
     */
    public function getForcePerso()
    {
        return $this->_forcePerso;
    }

    /**
     * @return mixed
     */
    public function getDegats()
    {
        return $this->_degats;
    }

    /**
     * @return mixed
     */
    public function getNiveau()
    {
        return $this->_niveau;
    }

    /**
     * @return mixed
     */
    public function getExperience()
    {
        return $this->_experience;
    }

    /**
     * @param $id
     */
    public function setId($id)
    {
        $this->_id = (int) $id;
    }

    /**
     * @param $nom
     */
    public function setNom($nom)
    {
        if (is_string($nom) && strlen($nom) <= 30)
        {
            $this->_nom = $nom;
        }
    }

    /**
     * @param $forcePerso
     */
    public function setForcePerso($forcePerso)
    {
        $forcePerso = (int) $forcePerso;

        if ($forcePerso >= 0 && $forcePerso <= 100)
        {
            $this->_forcePerso = $forcePerso;
        }
    }

    /**
     * @param $degats
     */
    public function setDegats($degats)
    {
        $degats = (int) $degats;

        if ($degats >= 0 && $degats <= 100)
        {
            $this->_degats = $degats;
        }
    }

    /**
     * @param $niveau
     */
    public function setNiveau($niveau)
    {
        $niveau = (int) $niveau;

        if ($niveau >= 0)
        {
            $this->_niveau = $niveau;
        }
    }

    /**
     * @param $exp
     */
    public function setExperience($exp)
    {
        $exp = (int) $exp;

        if ($exp >= 0 && $exp <= 500)
        {
            $this->_experience = $exp;
        }
    }
}